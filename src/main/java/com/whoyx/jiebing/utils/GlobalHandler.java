package com.whoyx.jiebing.utils;

import cn.dev33.satoken.exception.NotLoginException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

/**
 * @author stone
 */
@RestControllerAdvice
@Slf4j
public class GlobalHandler {

    @ExceptionHandler(NotLoginException.class)
    public CallResultMsg error(NotLoginException e) {
        log.warn("发生异常", e);
        return CallResultMsg.builder()
                .code(10000)
                .message("当前访问已失效，请重新进入！")
                .data(new HashMap<>(1))
                .build();
    }

    @ExceptionHandler(BaseException.class)
    public CallResultMsg error(BaseException e) {
        log.warn("发生异常", e);
        return CallResultMsg.builder()
                .code(e.getCode())
                .message(e.getMessage())
                .data(new HashMap<>(1))
                .build();
    }

    @ExceptionHandler(Throwable.class)
    public CallResultMsg error(Throwable e) {
        String result = ExceptionUtils.printErrorInfo(e);
        log.warn("发生异常", e);
        return CallResultMsg.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(result)
                .build();
    }

}


