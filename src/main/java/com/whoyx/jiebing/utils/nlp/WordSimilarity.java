package com.whoyx.jiebing.utils.nlp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WordSimilarity {

    private String word;

    private String keyWord;

    /**
     * 余弦相似度
     */
    private Float cosineSim;

    /**
     * 内积
     */
    private Float dot;
}
