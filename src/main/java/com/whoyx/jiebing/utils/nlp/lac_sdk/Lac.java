package com.whoyx.jiebing.utils.nlp.lac_sdk;

import ai.djl.repository.zoo.Criteria;
import ai.djl.training.util.ProgressBar;
import com.whoyx.jiebing.config.NlpFilePathConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author stone
 */
@Slf4j
@Component
@RequiredArgsConstructor
public final class Lac {

    private final NlpFilePathConfig pathConfig;

    public Criteria<String, String[][]> criteria() {
        Path lacPath = Paths.get(pathConfig.getLacPath());
        return Criteria.builder()
                .setTypes(String.class, String[][].class)
                .optModelPath(lacPath)
                .optTranslator(new LacTranslator())
                .optProgress(new ProgressBar())
                .optEngine("PaddlePaddle")
                .build();
    }

}
