package com.whoyx.jiebing.utils;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author stone
 */
@Data
@Builder
public class PageResult<T> {

    /**
     * 页码
     */
    private long page;

    /**
     * 每页条数
     */
    private long size;

    /**
     * 总条数
     */
    private long total;

    /**
     * 结果集
     */
    private List<T> list;

}
