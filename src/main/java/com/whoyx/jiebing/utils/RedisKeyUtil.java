package com.whoyx.jiebing.utils;

public class RedisKeyUtil {
    public static String userInfoKey(String uid){
        return "jiebing_user_info_" + uid;
    }

    public static String OutPatientServiceInfo(String uid) {
        return "jiebing_outpatient_info_" + uid;
    }
}
