package com.whoyx.jiebing.utils;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhangxueqiang
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class BaseException extends RuntimeException{

    private int code;
    private String message;

    public BaseException(int code, String msg) {
        super(msg);
        this.code = code;
        this.message = msg;
    }
}
