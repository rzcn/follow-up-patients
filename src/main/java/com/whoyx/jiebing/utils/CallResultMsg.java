package com.whoyx.jiebing.utils;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Stone
 * @date 2021/7/15
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class CallResultMsg {
    private Integer code;
    private String message;
    private Object data;
}
