package com.whoyx.jiebing.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

/**
 * @author Stone
 * @date 2021/7/15
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ClassUtils {

    public static Method getMethod(Class<?> clazz, String methodName) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        return null;
    }
}
