package com.whoyx.jiebing.config;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import cn.dev33.satoken.strategy.SaStrategy;
import com.whoyx.jiebing.utils.StpAdminUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Stone
 * @date 2022/5/6
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    /**
     * 为 StpUserUtil 注入 StpLogicJwt 实现
     */
    @Autowired
    public void setAdminStpLogic() {
        SaManager.putStpLogic(StpAdminUtils.stpLogic);
    }

    @Autowired
    public void rewriteSaStrategy() {
        // 重写Sa-Token的注解处理器，增加注解合并功能
        SaStrategy.me.getAnnotation = AnnotatedElementUtils::getMergedAnnotation;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册注解拦截器，并排除不需要注解鉴权的接口地址 (与登录拦截器无关)
        registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**");
    }

}
