package com.whoyx.jiebing.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author stone
 */
@Data
@Component
@ConfigurationProperties(prefix = "nlp")
public class NlpFilePathConfig {

    private String lacPath;

    private String wordEncoderNpyPath;

    private String wordEncoderTxtPath;

    private String sentenceEncoderPath;

}
