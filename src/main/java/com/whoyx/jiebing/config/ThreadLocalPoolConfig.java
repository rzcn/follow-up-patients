package com.whoyx.jiebing.config;

import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadLocalPoolConfig {
    public static ExecutorService executorService = Executors.newFixedThreadPool(10);
}
