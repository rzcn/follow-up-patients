package com.whoyx.jiebing.config;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.Scanner;

/**
 * 数据表代码生成工具
 * 1.运行main方法
 * 2.控制台输入数据库表名
 * 3.在controller,service,service.impl,dao,dao.mapper,domain.entity包内会生成相应的代码
 * 4.重复运行后，检测到对应包中已有类，不再生成
 * @author stone
 */
public class CodeGeneratorUtil {
    /**读取控制台内容*/
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) {
        String projectPath = System.getProperty("user.dir");

        FastAutoGenerator.create("jdbc:mysql://rm-bp18vwy31p1io1862co.mysql.rds.aliyuncs.com:3306/follow_up_patients?useUnicode=true&characterEncoding=UTF-8&useSSL=true&serverTimezone=Asia/Shanghai", "gnahz", "WGD0wbv1mfLIAWEE")
                .globalConfig(builder -> builder.author("stone")
                        .disableOpenDir()
                        .outputDir(projectPath + "/src/main/java")
                        .dateType(DateType.TIME_PACK)
                        .commentDate("yyyy-MM-dd"))
                .packageConfig(builder -> builder.parent("com.whoyx.jiebing")
                        .entity("dao.entity")
                        .service("dao.service")
                        .serviceImpl("dao.service.impl")
                        .mapper("dao.mapper")
                        .xml("dao.mapper.xml"))
                .strategyConfig((scanner, builder) -> builder.addInclude(scanner("表名，多个英文逗号分割").split(","))
                        .addTablePrefix("t")
                        .entityBuilder()
                        .idType(IdType.AUTO)
                        .naming(NamingStrategy.underline_to_camel)
                        .columnNaming(NamingStrategy.underline_to_camel)
                        .enableLombok()
                        .enableTableFieldAnnotation()
                        .formatFileName("%sEntity")
                        .controllerBuilder()
                        .formatFileName("%sController")
                        .enableRestStyle()
                        .serviceBuilder()
                        .formatServiceFileName("I%sService")
                        .formatServiceImplFileName("I%sServiceImpl")
                        .mapperBuilder()
                        .formatMapperFileName("%sMapper")
                        .formatXmlFileName("%sMapper"))
                .execute();
    }
}
