package com.whoyx.jiebing.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.whoyx.jiebing.domain.msg.req.MsgUserRegisterReqDto;
import com.whoyx.jiebing.domain.msg.resp.TimResultMsg;
import com.whoyx.jiebing.domain.msg.resp.UserSigRespDto;
import com.whoyx.jiebing.service.MsgService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/msg")
public class MsgController {
    private final MsgService msgService;


    /**
     * 即时通信-回调
     * @param reqDto 请求参数
     * @return 请求结果
     */
    @PostMapping("/callback")
    public TimResultMsg msgCallback(@RequestBody Object reqDto) {
        msgService.callBack(reqDto);
        return TimResultMsg.builder()
                .ActionStatus("OK")
                .ErrorCode(0)
                .ErrorInfo("")
                .build();
    }

    /**
     * 即时通信-用户注册
     * @param reqDto 请求参数
     */
    @PostMapping("/user/register")
    public TimResultMsg register(@RequestBody MsgUserRegisterReqDto reqDto){
        return msgService.register(reqDto);
    }

    /**
     * 注册客服用户
     */
    @PostMapping("/customer-service/register")
    public void customerServiceRegister(){
        msgService.customerServiceRegister();
    }

    /**
     * 获取userSig
     */
    @SaCheckLogin
    @GetMapping("/user-sig")
    public UserSigRespDto getUserSig(){
        return msgService.genUserSig();
    }

    /**
     * 人工咨询调用接口
     */
    @SaCheckLogin
    @GetMapping("/manual-consultation")
    public void manualConsultation(){
        msgService.manualConsultation(null);
    }

}
