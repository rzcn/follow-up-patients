package com.whoyx.jiebing.controller;

import com.whoyx.jiebing.domain.req.ServiceInitReqDto;
import com.whoyx.jiebing.domain.resp.ServiceInitRespDto;
import com.whoyx.jiebing.service.MsgService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/user/")
@RequiredArgsConstructor
public class UserController {
    private final MsgService msgService;

    /**
     * 服务初始化接口
     * @param reqDto 请求参数
     * @return 初始化内容
     */
    @PostMapping("/service/init")
    public ServiceInitRespDto serviceInit(@RequestBody @Validated ServiceInitReqDto reqDto){
        return msgService.serviceInit(reqDto);
    }


}
