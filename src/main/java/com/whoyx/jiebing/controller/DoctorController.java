package com.whoyx.jiebing.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.whoyx.jiebing.domain.BasePageReqDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import com.whoyx.jiebing.domain.req.DoctorAddCommonWordReqDto;
import com.whoyx.jiebing.domain.req.DoctorCommonWordModifyReqDto;
import com.whoyx.jiebing.domain.req.MsgRecordPageReqDto;
import com.whoyx.jiebing.domain.resp.ConsultDataRespDto;
import com.whoyx.jiebing.domain.resp.DoctorCommonWordRespDto;
import com.whoyx.jiebing.domain.resp.UploadUrlRespDto;
import com.whoyx.jiebing.service.DoctorService;
import com.whoyx.jiebing.service.MsgService;
import com.whoyx.jiebing.utils.PageResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhangxueqiang
 */
@CrossOrigin
@SaCheckLogin
@RestController
@RequiredArgsConstructor
@RequestMapping("/doctor")
public class DoctorController {
    private final DoctorService doctorService;
    private final MsgService msgService;

    /**
     * 新增常用语
     * @param reqDto 请求参数
     */
    @PostMapping("/common-word/add")
    public void addCommonWord(@RequestBody DoctorAddCommonWordReqDto reqDto) {
        doctorService.addCommonWord(reqDto);
    }

    /**
     * 查询常用语列表
     * @param reqDto 请求参数
     * @return {@link DoctorCommonWordRespDto}
     */
    @GetMapping("/common-word/page")
    public PageResult<DoctorCommonWordRespDto> commonWordPage(@ModelAttribute BasePageReqDto reqDto){
        return doctorService.CommonWordPage(reqDto);
    }

    /**
     * 编辑常用语
     * @param reqDto 请求参数
     */
    @PostMapping("/common-word/modify")
    public void commonWordModify(@RequestBody DoctorCommonWordModifyReqDto reqDto){
        doctorService.commonWordModify(reqDto);
    }

    /**
     * 删除常用语
     * @param commonWordId 常用语id
     */
    @PostMapping("/common-word/delete/{commonWordId}")
    public void commonWordDelete(@PathVariable Integer commonWordId) {
        doctorService.commonWordDelete(commonWordId);
    }

    /**
     * 医生与患者聊天记录
     * @param reqDto 请求参数
     * @return {@link MsgDataRespDto}
     */
    @GetMapping("/msg/patients-doctor/page")
    public PageResult<MsgDataRespDtoV2> msgRecordDoctorPatient(@ModelAttribute MsgRecordPageReqDto reqDto){
        return msgService.msgRecordDoctorPatient(reqDto);
    }

    /**
     * 客服与患者聊天记录
     * @param reqDto 请求参数
     * @return {@link MsgDataRespDtoV2}
     */
    @GetMapping("/msg/patients-service/page")
    public PageResult<MsgDataRespDtoV2> msgRecordServicePatient(@ModelAttribute MsgRecordPageReqDto reqDto){
        return msgService.msgRecordServicePatient(reqDto);
    }

    /**
     * 咨询列表
     * @return {@link ConsultDataRespDto}
     */
    @GetMapping("/consult/list")
    public List<ConsultDataRespDto> consultList(){
        return doctorService.consultList();
    }

    /**
     * 数据上传链接
     * @param patUid 用户uid
     * @return {@link UploadUrlRespDto}
     */
    @GetMapping("/upload/url")
    public UploadUrlRespDto uploadUrl(@RequestParam String patUid){
        return doctorService.uploadUrl(patUid);
    }

}
