package com.whoyx.jiebing.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.whoyx.jiebing.domain.BasePageReqDto;
import com.whoyx.jiebing.domain.msg.resp.InitConversationRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import com.whoyx.jiebing.domain.resp.*;
import com.whoyx.jiebing.service.MsgService;
import com.whoyx.jiebing.service.PatientService;
import com.whoyx.jiebing.utils.PageResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@SaCheckLogin
@RestController
@RequiredArgsConstructor
@RequestMapping("/patient")
public class PatientController {
    private final PatientService patientService;
    private final MsgService msgService;

    /**
     * 就诊须知
     * @return {@link MedicalAdviceRespDto}
     */
    @GetMapping("/medical-advice")
    public MedicalAdviceRespDto medicalAdvice(){
        return patientService.medicalAdvice();
    }

    /**
     * 出诊时间表
     * @return {@link OutpatientServiceRespDto}
     */
    @GetMapping("/outpatient-service/list")
    public List<OutpatientServiceRespDto> outpatientServiceInfo(){
        return patientService.outpatientServiceInfo();
    }

    /**
     * 患者信息
     * @return {@link PatientBaseInfoRespDto}
     */
    @GetMapping("/base-info")
    public PatientBaseInfoRespDto baseInfo(){
        return patientService.baseInfo();
    }

    /**
     * 查询患者聊天记录
     * @return {@link MsgDataRespDto}
     */
    @GetMapping("/msg/msg-record/page")
    public PageResult<MsgDataRespDtoV2> msgRecordPage(@ModelAttribute BasePageReqDto reqDto){
        return msgService.patientMsgRecordPage(reqDto);
    }

    /**
     * 科普早知道
     * @return {@link PopularScienceArticle}
     */
    @GetMapping("/popular-science-article")
    public List<PopularScienceArticle> popularScienceArticles(){
        return patientService.popularScienceArticles();
    }

    /**
     * 会话初始化接口
     * @return {@link InitConversationRespDto}
     */
    @GetMapping("/init/conversation")
    public InitConversationRespDto initConversation(){
        return msgService.initConversation();
    }

}
