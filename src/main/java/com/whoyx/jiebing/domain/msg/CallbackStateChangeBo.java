package com.whoyx.jiebing.domain.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CallbackStateChangeBo {

    @JsonProperty("CallbackCommand")
    private String CallbackCommand;
    @JsonProperty("EventTime")
    private Long EventTime;
    @JsonProperty("Info")
    private InfoDTO Info;
    @JsonProperty("KickedDevice")
    private List<KickedDeviceDTO> KickedDevice;

    @NoArgsConstructor
    @Data
    public static class InfoDTO {
        @JsonProperty("Action")
        private String Action;
        @JsonProperty("To_Account")
        private String To_Account;
        @JsonProperty("Reason")
        private String Reason;
    }

    @NoArgsConstructor
    @Data
    public static class KickedDeviceDTO {
        @JsonProperty("Platform")
        private String Platform;
    }
}
