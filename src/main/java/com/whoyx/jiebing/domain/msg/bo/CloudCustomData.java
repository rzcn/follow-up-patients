package com.whoyx.jiebing.domain.msg.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudCustomData {
    /**
     * 类型
     * 1、人工咨询
     * 2、问卷评估
     * 3、上传图片
     * 4 结束服务
     * 5、开启人工咨询服务
     * 6、结束咨询
     * 7 猜你想看
     * 8、专家介绍
     * 9、出诊时间
     * 10、就诊须知
     * 11、会话超时 结束咨询
     */
    private Integer type;

    /**
     * 内容
     */
    private String content;

    /**
     * 链接
     */
    private String url;

    /**
     * 图片上传状态  当type为2、3、4时不为空
     * 0、未上传
     * 1、已上传
     */
    private Integer status;

    private String data;
}
