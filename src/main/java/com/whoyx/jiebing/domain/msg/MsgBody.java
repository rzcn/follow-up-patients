package com.whoyx.jiebing.domain.msg;

import com.whoyx.jiebing.domain.enums.MsgTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MsgBody {
    /**
     * @see MsgTypeEnum
     */
    private String msgType;

    private Object msgContent;
}
