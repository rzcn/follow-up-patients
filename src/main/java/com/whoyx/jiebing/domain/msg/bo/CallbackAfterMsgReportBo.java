package com.whoyx.jiebing.domain.msg.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallbackAfterMsgReportBo {

    @JsonProperty("CallbackCommand")
    private String CallbackCommand;

    @JsonProperty("Report_Account")
    private String Report_Account;
    @JsonProperty("Peer_Account")
    private String Peer_Account;
    @JsonProperty("LastReadTime")
    private Integer LastReadTime;
    @JsonProperty("UnreadMsgNum")
    private Integer UnreadMsgNum;
}
