package com.whoyx.jiebing.domain.msg.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSigRespDto {
    /**
     * 用户唯一标识
     */
    private String uid;

    /**
     * userSig
     */
    private String userSig;

    /**
     * 过期时间 单位秒
     */
    private Long expire;
}
