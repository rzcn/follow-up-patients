package com.whoyx.jiebing.domain.msg.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CallbackAfterMsgWithDrawBo {

    /**
     * 回调命令
     */
    @JsonProperty("CallbackCommand")
    private String CallbackCommand;

    /**
     * 消息发送者 UserID
     */
    @JsonProperty("From_Account")
    private String From_Account;

    /**
     * 消息接收者 UserID
     */
    @JsonProperty("To_Account")
    private String To_Account;

    /**
     * 该条消息的唯一标识
     */
    @JsonProperty("MsgKey")
    private String MsgKey;

    /**
     * To_Account 未读的单聊消息总数量（包含所有的单聊会话）
     */
    @JsonProperty("UnreadMsgNum")
    private Integer UnreadMsgNum;
}
