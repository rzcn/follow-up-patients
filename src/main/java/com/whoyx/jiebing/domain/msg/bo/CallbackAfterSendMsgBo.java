package com.whoyx.jiebing.domain.msg.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.whoyx.jiebing.domain.msg.MsgBody;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallbackAfterSendMsgBo {
    @JsonProperty("CallbackCommand")
    private String CallbackCommand;
    @JsonProperty("From_Account")
    private String From_Account;
    @JsonProperty("To_Account")
    private String To_Account;
    @JsonProperty("MsgSeq")
    private Long MsgSeq;
    @JsonProperty("MsgRandom")
    private Long MsgRandom;
    @JsonProperty("MsgTime")
    private Long MsgTime;
    @JsonProperty("MsgKey")
    private String MsgKey;
    @JsonProperty("OnlineOnlyFlag")
    private Integer OnlineOnlyFlag;
    @JsonProperty("SendMsgResult")
    private Integer SendMsgResult;
    @JsonProperty("ErrorInfo")
    private String ErrorInfo;
    @JsonProperty("UnreadMsgNum")
    private Integer UnreadMsgNum;
    @JsonProperty("MsgBody")
    private List<Map<String,Object>> MsgBody;
    @JsonProperty("CloudCustomData")
    private String CloudCustomData;

}
