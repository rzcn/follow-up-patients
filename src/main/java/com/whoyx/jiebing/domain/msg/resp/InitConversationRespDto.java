package com.whoyx.jiebing.domain.msg.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InitConversationRespDto {

    /**
     * 是否发送欢迎语
     * 0、不发送 需要查看历史记录
     * 1、发送 不需要查看历史记录
     */
    private Integer status;

    private String doctorInfoUrl;
}
