package com.whoyx.jiebing.domain.msg.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TIMCustomData {
    @JsonProperty("Data")
    private String Data;

    @Builder.Default
    @JsonProperty("Desc")
    private String Desc = "";
    @Builder.Default
    @JsonProperty("Ext")
    private String Ext = "";
    @Builder.Default
    @JsonProperty("Sound")
    private String Sound = "";
}
