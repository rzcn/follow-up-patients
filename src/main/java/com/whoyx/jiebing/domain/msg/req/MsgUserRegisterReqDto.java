package com.whoyx.jiebing.domain.msg.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MsgUserRegisterReqDto {
    /**
     * openId
     */
    @NotBlank(message = "openId 不能为空")
    private String openId;

    /**
     * 用户角色
     * 1、医生
     * 2、患者
     */
    @NotNull(message = "用户角色不能为空")
    private Integer role;

    /**
     * 头像链接
     */
    private String faceUrl;

}
