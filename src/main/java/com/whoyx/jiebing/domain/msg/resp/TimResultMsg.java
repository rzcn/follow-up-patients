package com.whoyx.jiebing.domain.msg.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimResultMsg {
    private String ActionStatus;

    private String ErrorInfo;

    private Integer ErrorCode;
}
