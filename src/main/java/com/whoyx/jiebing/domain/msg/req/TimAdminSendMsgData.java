package com.whoyx.jiebing.domain.msg.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.whoyx.jiebing.domain.msg.MsgBody;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimAdminSendMsgData {

    @JsonProperty("SyncOtherMachine")
    private Integer syncOtherMachine;
    @JsonProperty("From_Account")
    private String fromAccount;
    @JsonProperty("To_Account")
    private String toAccount;
    @JsonProperty("MsgLifeTime")
    private Integer msgLifeTime;
    @JsonProperty("MsgSeq")
    private Long msgSeq;
    @JsonProperty("MsgRandom")
    private Long msgRandom;
    @JsonProperty("MsgBody")
    private List<MsgBody> msgBody;

    private List<String> forbidCallbackControl;

    private String cloudCustomData;
}
