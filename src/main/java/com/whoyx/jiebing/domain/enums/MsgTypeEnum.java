package com.whoyx.jiebing.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum MsgTypeEnum {
    TIMTextElem("TIMTextElem","文本消息"),
    TIMLocationElem("TIMLocationElem","地理位置消息"),
    TIMFaceElem("TIMFaceElem","表情消息"),
    TIMCustomElem("TIMCustomElem","自定义消息，当接收方为 iOS 系统且应用处在后台时，此消息类型可携带除文本以外的字段到 APNs。一条组合消息中只能包含一个 TIMCustomElem 自定义消息元素"),
    TIMSoundElem("TIMSoundElem","语音消息"),
    TIMImageElem("TIMImageElem","图像消息"),
    TIMFileElem("TIMFileElem","文件消息"),
    TIMVideoFileElem("TIMVideoFileElem","视频消息"),
    ;
    @Getter
    private final String name;

    @Getter
    private final String cnName;
}
