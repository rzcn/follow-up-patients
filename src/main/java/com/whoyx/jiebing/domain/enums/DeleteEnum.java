package com.whoyx.jiebing.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhangxueqiang
 */
@AllArgsConstructor
public enum DeleteEnum {
    /**
     * 标记删除
     */
    ENABLE(0L),
    UNABLE(System.currentTimeMillis()),
    ;
    @Getter
    private final Long value;
}
