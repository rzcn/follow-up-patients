package com.whoyx.jiebing.domain.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ServiceInitReqDto {
    /**
     * openId
     */
    @NotBlank(message = "openId 不能为空")
    private String openId;

    /**
     * 用户角色
     * 1、医生
     * 2、患者
     */
    @NotNull(message = "用户角色不能为空")
    private Integer role;
}
