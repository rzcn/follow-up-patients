package com.whoyx.jiebing.domain.req;

import lombok.Data;

@Data
public class DoctorAddCommonWordReqDto {
    /**
     * 常用语内容
     */
    private String content;
}
