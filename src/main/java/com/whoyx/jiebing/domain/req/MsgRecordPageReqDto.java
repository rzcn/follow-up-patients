package com.whoyx.jiebing.domain.req;

import com.whoyx.jiebing.domain.BasePageReqDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MsgRecordPageReqDto extends BasePageReqDto {
    /**
     * 患者uid
     */
    private String patientUid;
}
