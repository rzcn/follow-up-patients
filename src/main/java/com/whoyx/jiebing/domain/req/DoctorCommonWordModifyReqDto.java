package com.whoyx.jiebing.domain.req;

import lombok.Data;

@Data
public class DoctorCommonWordModifyReqDto {
    /**
     * 常用语id
     */
    private Integer commonWordId;

    /**
     * 常用语内容
     */
    private String content;
}
