package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PopularScienceArticle {
    /**
     * 文章标题
     */
    private String title;

    /**
     * 文章链接
     */
    private String url;
}
