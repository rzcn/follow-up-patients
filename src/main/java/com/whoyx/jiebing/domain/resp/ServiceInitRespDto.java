package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceInitRespDto {
    /**
     * 用户openId
     */
    private String openId;

    /**
     * 用户标识
     */
    private String uid;

    /**
     * token
     */
    private String token;

//    private String uploadPhotoUrl;
//
//    private String questionUrl;
}
