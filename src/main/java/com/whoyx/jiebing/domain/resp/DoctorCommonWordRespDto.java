package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoctorCommonWordRespDto {
    /**
     * 常用语id
     */
    private Integer commWordId;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建时间
     */
    private String createTime;
}
