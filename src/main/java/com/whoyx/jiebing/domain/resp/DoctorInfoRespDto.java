package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoctorInfoRespDto {
    /**
     * 医院名称
     */
    private String hospName;

    /**
     * 医生名称
     */
    private String drName;

    /**
     * 医生头像
     */
    private String drHeadUrl;

    /**
     * 职称
     */
    private String position;

    /**
     * 称号
     */
    private String title;

    /**
     * 职业经理
     */
    private String professionalHistory;

    /**
     * 社会任职
     */
    private String socialService;

    /**
     * 擅长
     */
    private String skill;

    /**
     * 发表时间
     */
    private String publicationTime;
}
