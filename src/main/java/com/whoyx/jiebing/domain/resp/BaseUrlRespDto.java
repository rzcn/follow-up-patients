package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseUrlRespDto {
    /**
     * 专家介绍链接
     */
    private String url;
}
