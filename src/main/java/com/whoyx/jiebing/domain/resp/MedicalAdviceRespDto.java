package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicalAdviceRespDto {
    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;
}
