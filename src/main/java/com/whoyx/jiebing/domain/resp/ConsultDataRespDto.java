package com.whoyx.jiebing.domain.resp;

import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsultDataRespDto {
    /**
     * 姓名
     */
    private String name;

    /**
     * 头像
     */
    private String headUrl;

    /**
     * 患者uid
     */
    private String uid;

    /**
     * 最新消息
     */
    private MsgDataRespDtoV2 recentMsgData;

    /**
     * 点击头像跳转链接
     */
    private String userInfoUrl;

    /**
     * 未读消息计数
     */
    private Integer unReadNum;

    /**
     * 咨询类型
     * 0 客服
     * 1 医生
     */
    private Integer type;



}
