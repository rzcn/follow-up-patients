package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PatientBaseInfoRespDto {

    /**
     * 患者微信openId
     */
    private String patKey;

    /**
     * 患者姓名
     */
    private String patName;

    /**
     * 最新落实
     */
    private String lastConfirm;

    /**
     * 最新患者提交预约日期
     */
    private String lastBooking;

    /**
     * 患者就诊卡号
     */
    private String patNumber;

    /**
     * 患者病历号
     */
    private String patDocument;

    /**
     * 患者性别
     */
    private String patGender;

    /**
     * 患者年龄
     */
    private String age;

    /**
     * 患者手机号
     */
    private String mobile;

    /**
     * 患者类型：手术或非手术
     */
    private String lastIsSurg;

    /**
     * 最新随访登记日期
     */
    private String lastPatCheckin;

    /**
     * 最新问卷/随访登记日期 若无此参数，表示患者未做问卷、随访登记
     */
    private String lastPatFill;

    /**
     * 最新审核结果 若无此参数，或值为空，则表示从未做过审核
     */
    private String lastAudit;

    /**
     * 未到复诊时间 无值时表示医生未决定 值：yes
     */
    private String notNextAppoint;

    /**
     * 下次复诊日期 not_next_appoint为yes时才有，格式yyyy-MM-dd
     */
    private String nextAppointDate;

    /**
     * 最新颈部B超报告拍照 若无此参数，表示患者未上传颈部B超报告拍照
     */
    private String lastPicNeckbu;

    /**
     * 最新手术记录报告 若无此参数，表示患者未上传手术记录报告拍照
     */
    private String lastPicSurgReport;

    private String lastDiagType;

    /**
     * 最新诊断日期 若无此参数，表示医生未登记诊断
     */
    private String lastDiagDate;
}
