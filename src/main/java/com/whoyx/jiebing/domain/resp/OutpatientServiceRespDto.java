package com.whoyx.jiebing.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OutpatientServiceRespDto {
    /**
     * 医生姓名
     */
    private String drName;

    /**
     * 医生头衔
     */
    private String title;

    /**
     * 出诊科室
     */
    private String department;

    /**
     * 院区
     */
    private String location;

    /**
     * 出诊日期 yyyy-MM-dd
     */
    private String clinicDate;

    /**
     * 星期几
     */
    private String dayOfWeek;

    /**
     * 时段  上午、下午、夜间
     */
    private String timeOfDay;

    /**
     * 医事服务费
     */
    private String fee;

    /**
     * 状态 正常、停诊
     */
    private String status;
}
