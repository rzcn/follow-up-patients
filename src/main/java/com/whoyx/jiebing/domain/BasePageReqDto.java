package com.whoyx.jiebing.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author zhangxueqiang
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasePageReqDto {
    /**
     * 页码
     */
    @NotNull(message = "页码不能为空")
    @Min(value = 1, message = "页码最小为1")
    private Integer page;

    /**
     * 大小
     */
    @NotNull(message = "每页条数不能为空")
    @Min(value = 1, message = "每页最少1条")
    @Max(value = 100)
    private Integer size;
}
