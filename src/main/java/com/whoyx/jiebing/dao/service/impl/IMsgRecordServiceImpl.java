package com.whoyx.jiebing.dao.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whoyx.jiebing.dao.entity.MsgRecordEntity;
import com.whoyx.jiebing.dao.entity.UserEntity;
import com.whoyx.jiebing.dao.mapper.MsgRecordMapper;
import com.whoyx.jiebing.dao.service.IMsgRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import com.whoyx.jiebing.utils.PageResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 消息记录表 服务实现类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Service
public class IMsgRecordServiceImpl extends ServiceImpl<MsgRecordMapper, MsgRecordEntity> implements IMsgRecordService {

    @Value("${tim.serviceUid}")
    private String serviceUid;


    @Override
    public PageResult<MsgDataRespDtoV2> msgRecordByConversation(Integer conversationId, Integer pages, Integer size, String representUid, UserEntity userEntity, String serviceAvatar, String drAvatar) {
        Page<MsgRecordEntity> page = Page.of(pages, size);
        Page<MsgRecordEntity> msgRecordEntityPage = this.page(page,
                Wrappers.<MsgRecordEntity>lambdaQuery()
                        .eq(MsgRecordEntity::getConversationId, conversationId)
                        .orderByDesc(MsgRecordEntity::getMsgTime, MsgRecordEntity::getMsgTime)
        );
        List<MsgDataRespDtoV2> respList = msgRecordEntityPage.getRecords().stream()
                .map(i -> {
                    String fromAvatar;
                    if (i.getFromUid().equals(serviceUid)) {
                        fromAvatar = serviceAvatar;
                    } else if (i.getFromUid().equals(userEntity.getUid())) {
                        fromAvatar = userEntity.getHeaderUrl();
                    } else {
                        fromAvatar = drAvatar;
                    }
                    return MsgDataRespDtoV2.buildWithEntity(i, representUid, fromAvatar);
                }).collect(Collectors.toList());
        respList.sort(Comparator.comparing(MsgDataRespDtoV2::getTime));
        return PageResult.<MsgDataRespDtoV2>builder()
                .page(pages)
                .size(size)
                .total(msgRecordEntityPage.getTotal())
                .list(respList)
                .build();
    }

    @Override
    public PageResult<MsgDataRespDtoV2> msgRecordByUid(UserEntity userEntity, Integer pages, Integer size, String serviceAvatar, String drAvatar) {
        String representUid = userEntity.getUid();
        Page<MsgRecordEntity> page = Page.of(pages, size);
        Page<MsgRecordEntity> msgRecordEntityPage = page(page, Wrappers.<MsgRecordEntity>lambdaQuery()
                .and(w -> w.eq(MsgRecordEntity::getFromUid, representUid)
                        .or().eq(MsgRecordEntity::getToUid, representUid))
//                .gt(MsgRecordEntity::getMsgTime, userEntity.getTargetUpdateTime())
                .orderByDesc(MsgRecordEntity::getMsgTime)
        );

        return PageResult.<MsgDataRespDtoV2>builder()
                .size(size)
                .page(pages)
                .total(msgRecordEntityPage.getTotal())
                .list(msgRecordEntityPage.getRecords().stream().sorted(Comparator.comparing(MsgRecordEntity::getMsgTime))
                        .map(i -> {
                            String fromAvatar;
                            if (i.getFromUid().equals(serviceUid)) {
                                fromAvatar = serviceAvatar;
                            } else if (i.getFromUid().equals(userEntity.getUid())) {
                                fromAvatar = userEntity.getHeaderUrl();
                            } else {
                                fromAvatar = drAvatar;
                            }
                            return MsgDataRespDtoV2.buildWithEntity(i, representUid, fromAvatar);
                        }).collect(Collectors.toList()))
                .build();
    }



}
