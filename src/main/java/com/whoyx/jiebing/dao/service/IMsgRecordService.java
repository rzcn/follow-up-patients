package com.whoyx.jiebing.dao.service;

import com.whoyx.jiebing.dao.entity.MsgRecordEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whoyx.jiebing.dao.entity.UserEntity;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import com.whoyx.jiebing.utils.PageResult;

/**
 * <p>
 * 消息记录表 服务类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface IMsgRecordService extends IService<MsgRecordEntity> {

    PageResult<MsgDataRespDtoV2> msgRecordByConversation(Integer conversationId, Integer pages, Integer size, String representUid, UserEntity userEntity, String serviceAvatar, String drAvatar);

    PageResult<MsgDataRespDtoV2> msgRecordByUid(UserEntity userEntity, Integer pages, Integer size, String serviceAvatar, String drAvatar);
}
