package com.whoyx.jiebing.dao.service;

import com.whoyx.jiebing.dao.entity.MsgConversationEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会话id表 服务类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface IMsgConversationService extends IService<MsgConversationEntity> {

}
