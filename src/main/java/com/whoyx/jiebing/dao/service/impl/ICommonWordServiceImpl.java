package com.whoyx.jiebing.dao.service.impl;

import com.whoyx.jiebing.dao.entity.CommonWordEntity;
import com.whoyx.jiebing.dao.mapper.CommonWordMapper;
import com.whoyx.jiebing.dao.service.ICommonWordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常用语表 服务实现类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Service
public class ICommonWordServiceImpl extends ServiceImpl<CommonWordMapper, CommonWordEntity> implements ICommonWordService {

}
