package com.whoyx.jiebing.dao.service;

import com.whoyx.jiebing.dao.entity.UserServiceWordEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 智能客服回复语表 服务类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface IUserServiceWordService extends IService<UserServiceWordEntity> {

}
