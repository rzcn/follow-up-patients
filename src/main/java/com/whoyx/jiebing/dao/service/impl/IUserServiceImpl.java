package com.whoyx.jiebing.dao.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.whoyx.jiebing.dao.entity.UserEntity;
import com.whoyx.jiebing.dao.mapper.UserMapper;
import com.whoyx.jiebing.dao.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whoyx.jiebing.domain.enums.DeleteEnum;
import com.whoyx.jiebing.utils.BaseException;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Service
public class IUserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements IUserService {

    @Override
    public UserEntity getByOpenId(String openId) {
        return getOne(
                Wrappers.<UserEntity>lambdaQuery()
                        .eq(UserEntity::getOpenId, openId)
                        .eq(UserEntity::getDeleted, DeleteEnum.ENABLE.getValue())
        );
    }

    @Override
    public UserEntity validatedEntityByUid(String uid) {
        UserEntity userEntity = getOne(
                Wrappers.<UserEntity>lambdaQuery()
                        .eq(UserEntity::getUid, uid)
                        .eq(UserEntity::getDeleted, DeleteEnum.ENABLE.getValue())
        );
        return validatedUserEntity(userEntity);
    }

    private UserEntity validatedUserEntity(UserEntity userEntity) {
        if (userEntity == null) {
            throw new BaseException(403,"用户不存在");
        }
        return userEntity;
    }
}
