package com.whoyx.jiebing.dao.service;

import com.whoyx.jiebing.dao.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface IUserService extends IService<UserEntity> {

    UserEntity getByOpenId(String openId);

    UserEntity validatedEntityByUid(String uid);
}
