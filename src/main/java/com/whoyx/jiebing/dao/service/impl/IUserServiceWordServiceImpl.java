package com.whoyx.jiebing.dao.service.impl;

import com.whoyx.jiebing.dao.entity.UserServiceWordEntity;
import com.whoyx.jiebing.dao.mapper.UserServiceWordMapper;
import com.whoyx.jiebing.dao.service.IUserServiceWordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 智能客服回复语表 服务实现类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Service
public class IUserServiceWordServiceImpl extends ServiceImpl<UserServiceWordMapper, UserServiceWordEntity> implements IUserServiceWordService {

}
