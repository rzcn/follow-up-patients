package com.whoyx.jiebing.dao.service.impl;

import com.whoyx.jiebing.dao.entity.MsgConversationEntity;
import com.whoyx.jiebing.dao.mapper.MsgConversationMapper;
import com.whoyx.jiebing.dao.service.IMsgConversationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会话id表 服务实现类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Service
public class IMsgConversationServiceImpl extends ServiceImpl<MsgConversationMapper, MsgConversationEntity> implements IMsgConversationService {

}
