package com.whoyx.jiebing.dao.service;

import com.whoyx.jiebing.dao.entity.CommonWordEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常用语表 服务类
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface ICommonWordService extends IService<CommonWordEntity> {

}
