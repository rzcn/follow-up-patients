package com.whoyx.jiebing.dao.mapper;

import com.whoyx.jiebing.dao.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface UserMapper extends BaseMapper<UserEntity> {

}
