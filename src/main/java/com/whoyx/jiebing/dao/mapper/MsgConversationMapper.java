package com.whoyx.jiebing.dao.mapper;

import com.whoyx.jiebing.dao.entity.MsgConversationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会话id表 Mapper 接口
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface MsgConversationMapper extends BaseMapper<MsgConversationEntity> {

}
