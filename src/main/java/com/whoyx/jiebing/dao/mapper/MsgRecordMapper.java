package com.whoyx.jiebing.dao.mapper;

import com.whoyx.jiebing.dao.entity.MsgRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息记录表 Mapper 接口
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface MsgRecordMapper extends BaseMapper<MsgRecordEntity> {

}
