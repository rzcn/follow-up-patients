package com.whoyx.jiebing.dao.mapper;

import com.whoyx.jiebing.dao.entity.CommonWordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常用语表 Mapper 接口
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface CommonWordMapper extends BaseMapper<CommonWordEntity> {

}
