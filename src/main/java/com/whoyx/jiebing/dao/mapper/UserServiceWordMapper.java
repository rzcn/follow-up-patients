package com.whoyx.jiebing.dao.mapper;

import com.whoyx.jiebing.dao.entity.UserServiceWordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 智能客服回复语表 Mapper 接口
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
public interface UserServiceWordMapper extends BaseMapper<UserServiceWordEntity> {

}
