package com.whoyx.jiebing.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 智能客服回复语表
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Getter
@Setter
@TableName("t_user_service_word")
public class UserServiceWordEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 触发关键字
     */
    @TableField("keyword")
    private String keyword;

    /**
     * 问题示例
     */
    @TableField("question_sample")
    private String questionSample;

    /**
     * 前置条件
     */
    @TableField("pre_condition")
    private Integer preCondition;

    /**
     * 回复语
     */
    @TableField("content")
    private String content;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableField("deleted")
    private Long deleted;
}
