package com.whoyx.jiebing.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 会话id表
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Getter
@Setter
@TableName("t_msg_conversation")
public class MsgConversationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 医生/客服（uid） + 患者（uid）
     */
    @TableField("conversation_string")
    private String conversationString;

    /**
     * 最新消息记录id
     */
    @TableField("recent_msg_id")
    private Long recentMsgId;

    /**
     * 最新消息时间
     */
    @TableField("recent_msg_time")
    private LocalDateTime recentMsgTime;

    @TableField("unread_msg_num")
    private Integer unreadMsgNum;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableField("deleted")
    private Long deleted;
}
