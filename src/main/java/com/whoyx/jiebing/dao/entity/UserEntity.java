package com.whoyx.jiebing.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Getter
@Setter
@TableName("t_user")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户标识 / 通信Id
     */
    @TableField("uid")
    private String uid;

    /**
     * 姓名
     */
    @TableField("name")
    private String name;

    /**
     * openId
     */
    @TableField("open_id")
    private String openId;

    /**
     * 头像链接
     */
    @TableField("header_url")
    private String headerUrl;

    /**
     * 用户当前聊天对象
     */
    @TableField("target_uid")
    private String targetUid;

    @TableField("target_update_time")
    private LocalDateTime targetUpdateTime;

    /**
     * 角色 1：医生 2：患者
     */
    @TableField("user_role")
    private Integer userRole;

    /**
     * 是否回答甲状腺相关症状
     */
    @TableField("thyroid_symptom")
    private Integer thyroidSymptom;

    /**
     * 需要发送问候语
     * 1、是
     * 2、否
     */
    @TableField("greeting_msg")
    private Integer greetingMsg;

    /**
     * 用户登录状态
     */
    @TableField("status")
    private String status;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableField("deleted")
    private Long deleted;
}
