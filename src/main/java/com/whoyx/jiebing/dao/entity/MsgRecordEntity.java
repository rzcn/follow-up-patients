package com.whoyx.jiebing.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 消息记录表
 * </p>
 *
 * @author stone
 * @since 2022-12-20
 */
@Getter
@Setter
@TableName("t_msg_record")
public class MsgRecordEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 会话id
     */
    @TableField("conversation_id")
    private Integer conversationId;

    /**
     * 发送方标识
     */
    @TableField("from_uid")
    private String fromUid;

    /**
     * 接收方标识
     */
    @TableField("to_uid")
    private String toUid;

    /**
     * 消息唯一标识
     */
    @TableField("msg_key")
    private String msgKey;

    /**
     * 消息序列号，用于标记该条消息（32位无符号整数）
     */
    @TableField("msg_random")
    private Long msgRandom;

    /**
     * 消息随机数，用于标记该条消息（32位无符号整数）
     */
    @TableField("msg_seq")
    private Long msgSeq;

    /**
     * To_Account 未读的单聊消息总数量（包含所有的单聊会话）。若该条消息下发失败（例如被脏字过滤），该字段值为-1
     */
    @TableField("unread_msg_num")
    private Integer unreadMsgNum;

    /**
     * 消息json
     */
    @TableField("msg_body")
    private String msgBody;

    /**
     * 自定义消息
     */
    @TableField("cloud_custom_data")
    private String cloudCustomData;

    /**
     * 消息时间
     */
    @TableField("msg_time")
    private LocalDateTime msgTime;

    /**
     * 数据上传状态
     */
    @TableField("update_status")
    private Integer updateStatus;


    /**
     * 消息时间戳
     */
    @TableField("time_stamp")
    private Long timeStamp;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableField("deleted")
    private Long deleted;
}
