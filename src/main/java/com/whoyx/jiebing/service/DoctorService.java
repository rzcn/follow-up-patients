package com.whoyx.jiebing.service;

import com.whoyx.jiebing.domain.BasePageReqDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import com.whoyx.jiebing.domain.req.DoctorAddCommonWordReqDto;
import com.whoyx.jiebing.domain.req.DoctorCommonWordModifyReqDto;
import com.whoyx.jiebing.domain.req.MsgRecordPageReqDto;
import com.whoyx.jiebing.domain.resp.BaseUrlRespDto;
import com.whoyx.jiebing.domain.resp.ConsultDataRespDto;
import com.whoyx.jiebing.domain.resp.DoctorCommonWordRespDto;
import com.whoyx.jiebing.domain.resp.UploadUrlRespDto;
import com.whoyx.jiebing.utils.PageResult;

import java.util.List;

public interface DoctorService {
    /**
     * 新增常用语
     * @param reqDto 请求参数
     */
    void addCommonWord(DoctorAddCommonWordReqDto reqDto);

    /**
     * 查询常用语列表
     * @param reqDto 请求参数
     * @return {@link DoctorCommonWordRespDto}
     */
    PageResult<DoctorCommonWordRespDto> CommonWordPage(BasePageReqDto reqDto);

    /**
     * 编辑常用语
     * @param reqDto 请求参数
     */
    void commonWordModify(DoctorCommonWordModifyReqDto reqDto);

    /**
     * 删除常用语
     * @param commonWordId 常用语id
     */
    void commonWordDelete(Integer commonWordId);

    /**
     * 咨询列表
     * @return {@link ConsultDataRespDto}
     */
    List<ConsultDataRespDto> consultList();

    BaseUrlRespDto questionnaireUrl(String patUid);

    BaseUrlRespDto uploadPictureUrl(String patUid);

    UploadUrlRespDto uploadUrl(String patUid);
}
