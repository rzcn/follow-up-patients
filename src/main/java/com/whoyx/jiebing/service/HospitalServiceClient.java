package com.whoyx.jiebing.service;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.whoyx.jiebing.domain.resp.OutpatientServiceRespDto;
import com.whoyx.jiebing.domain.resp.PatientBaseInfoRespDto;
import com.whoyx.jiebing.utils.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class HospitalServiceClient {
    @Value("${third.secretKey}")
    private String key;
    @Value("${third.debug}")
    private boolean thirdDebug;

    private final static String OUTPATIENT_SERVICE_URL = "https://b.yicloud.org/api2/ks/29/data/view";
    private final static String PATIENT_INFO_URL = "https://b.yicloud.org/api2/ks/30/data/view";
    private final static String SEND_WX_MSG_URL = "https://b.yicloud.org/api2/ks/34/data/command";
    private final static String REGISTER_STATUS_URL = "https://b.yicloud.org/dyn2/wx/2/1643/p/index";

    public PatientBaseInfoRespDto patientBaseInfo(String openId) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("nonce", UUID.randomUUID().toString().replace("-", "").substring(0, 8));
        paramsMap.put("opi", openId);
//        if (thirdDebug) {
//            paramsMap.put("opi", "o6fsjuM0BFN8SKxJE54MOBkwUA2s");
////            paramsMap.put("opi", "o6fsjuM0BFN8SKxJE54MOBkwUA2s1");
//        }
        String sign = SignUtils.generateSign(paramsMap, key);
        paramsMap.put("sign", sign);
        String respStr = RemoteUtils.sentUrlencodedPost(PATIENT_INFO_URL, paramsMap);
        JSONObject jsonObject = JSONUtil.parseObj(respStr);
        if (jsonObject.getInt("code") != 200) {
            throw new BaseException(jsonObject.getInt("code"), jsonObject.getStr("msg"));
        }
        PatientBaseInfoRespDto respDto = jsonObject.getBean("data", PatientBaseInfoRespDto.class);
        return respDto;
    }

    public List<OutpatientServiceRespDto> outpatientServiceInfo(String openId) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("nonce", UUID.randomUUID().toString().replace("-", "").substring(0, 8));
        paramsMap.put("opi", openId);
        if (thirdDebug) {
            paramsMap.put("opi", "owUEt1IgepxbkBTVNj8bTMkPlHio");
        }
        String sign = SignUtils.generateSign(paramsMap, key);
        paramsMap.put("sign", sign);
        String respStr = RemoteUtils.sentUrlencodedPost(OUTPATIENT_SERVICE_URL, paramsMap);
        JSONObject jsonObject = JSONUtil.parseObj(respStr);
        if (jsonObject.getInt("code") != 200) {
            throw new BaseException(jsonObject.getInt("code"), jsonObject.getStr("msg"));
        }
        return jsonObject.getBeanList("list", OutpatientServiceRespDto.class);
    }

    @Async
    public void sendWxMsg(String openId,String patName,String type){
        Map<String,String> paramsMap = new HashMap<>();
        paramsMap.put("type",type);
        paramsMap.put("opi", openId);
        if (StringUtils.isNotBlank(patName)) {
            paramsMap.put("pat_name", patName);
        }
        String sign = SignUtils.generateSign(paramsMap, key);
        paramsMap.put("sign", sign);
        RemoteUtils.sentUrlencodedPost(SEND_WX_MSG_URL, paramsMap);
    }
}
