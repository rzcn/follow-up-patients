package com.whoyx.jiebing.service;

import com.whoyx.jiebing.domain.BasePageReqDto;
import com.whoyx.jiebing.domain.msg.req.MsgUserRegisterReqDto;
import com.whoyx.jiebing.domain.msg.resp.*;
import com.whoyx.jiebing.domain.req.MsgRecordPageReqDto;
import com.whoyx.jiebing.domain.req.ServiceInitReqDto;
import com.whoyx.jiebing.domain.resp.ServiceInitRespDto;
import com.whoyx.jiebing.utils.PageResult;

public interface MsgService {
    /**
     * 消息回调
     * @param reqDto 回调信息
     */
    void callBack(Object reqDto);

    /**
     * 即时通信-用户注册
     * @param reqDto 请求参数
     * @return 请求结果
     */
    TimResultMsg register(MsgUserRegisterReqDto reqDto);

    /**
     * 服务初始化接口
     * @param reqDto 请求参数
     * @return 初始化内容
     */
    ServiceInitRespDto serviceInit(ServiceInitReqDto reqDto);

    /**
     * 注册客服用户
     */
    void customerServiceRegister();


    UserSigRespDto genUserSig();

    void manualConsultation(String patUid) ;

    InitConversationRespDto initConversation();

    PageResult<MsgDataRespDtoV2> patientMsgRecordPage(BasePageReqDto reqDto);

    /**
     * 医生与患者聊天记录
     * @param reqDto 请求参数
     * @return {@link MsgDataRespDto}
     */
    PageResult<MsgDataRespDtoV2> msgRecordDoctorPatient(MsgRecordPageReqDto reqDto);

    /**
     * 客服与患者聊天记录
     * @param reqDto 请求参数
     * @return {@link MsgDataRespDto}
     */
    PageResult<MsgDataRespDtoV2> msgRecordServicePatient(MsgRecordPageReqDto reqDto);
}
