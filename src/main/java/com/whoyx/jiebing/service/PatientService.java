package com.whoyx.jiebing.service;

import com.whoyx.jiebing.domain.BasePageReqDto;
import com.whoyx.jiebing.domain.msg.resp.InitConversationRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDto;
import com.whoyx.jiebing.domain.msg.resp.MsgDataRespDtoV2;
import com.whoyx.jiebing.domain.resp.*;
import com.whoyx.jiebing.utils.PageResult;

import java.util.List;

public interface PatientService {
    /**
     * 就诊须知
     * @return {@link MedicalAdviceRespDto}
     */
    MedicalAdviceRespDto medicalAdvice();

    /**
     * 出诊时间表
     * @return {@link OutpatientServiceRespDto}
     */
    List<OutpatientServiceRespDto> outpatientServiceInfo();

    List<OutpatientServiceRespDto> outpatientServiceInfo(String uid);

    /**
     * 患者信息
     * @return {@link PatientBaseInfoRespDto}
     */
    PatientBaseInfoRespDto baseInfo();

//    /**
//     * 查询患者聊天记录
//     * @return {@link MsgDataRespDto}
//     */
//    PageResult<MsgDataRespDtoV2> msgRecordPage(BasePageReqDto reqDto);

    /**
     * 专家介绍-url
     * @return {@link BaseUrlRespDto}
     */
    BaseUrlRespDto doctorInfo();

    /**
     * 问卷-url
     * @return {@link BaseUrlRespDto}
     */
    BaseUrlRespDto questionnaireUrl();

    /**
     * 上传图片-url
     * @return {@link BaseUrlRespDto}
     */
    BaseUrlRespDto uploadPictureUrl();

    /**
     * 科普早知道
     * @return {@link PopularScienceArticle}
     */
    List<PopularScienceArticle> popularScienceArticles();
}
